import numpy as np
from math import exp
import matplotlib.pyplot as plt
import trial_function as tf
import local_energy as E_loc
import metropolis as mp
import minimization
import time
import plot_figures as pf
import print_result as pr

def execute(N,alpha,h,Nw,gamma,e,name,method):
    """This function is the main executing file of the program. Depending on the input paramters this function determines which part of the code is     executed, and returns figures or txt files with results.
    ------------
    Input parameters:
    N: number of steps taken by the random walker   
    Nw: number of random walkers  
    alpha: variational parameter  
    h: order of stepsize  
    gamma: minimization step size multiplier (only relevant in the case of 'min')  
    e: desired precision of the result (only relevant in the case of 'min')
    name: system for which the calculation is desired ('HO', 'Hydrogen', 'HeliumNI', 'Helium')  
    method: way of finding the minimum value ('graph' or 'min')
    """
    if method == 'graph':
        N_var= alpha.size
        E_var = np.zeros(N_var)
        E_avg = np.zeros(N_var)
        accep_ratio = np.zeros(N_var)
        if name == 'HO':
            start_time = time.time()            
            for i in range(N_var):
                [x,accep_ratio[i]]=mp.metropolis(N,alpha[i],h[i])    
                E = E_loc.HO(x,alpha[i])
                E_avg[i] = 1/N*np.sum(E)
                E_var[i] = np.var(E)

            pf.fig_HO(alpha, E_avg, E_var, accep_ratio)
            print("1--- %s seconds ---" %(time.time() - start_time))
            
        elif name == 'Hydrogen':
            start_time = time.time()
            for i in range(N_var):
                [r,accep_ratio[i]] = mp.H(N,alpha[i],h[i])
                E = E_loc.H(r,alpha[i])
                E_avg[i] = 1/N*np.sum(E)
                E_var[i] = np.var(E)

            pf.fig_H(alpha, E_avg, E_var, accep_ratio)
            print("1--- %s seconds ---" %(time.time() - start_time))
            
        elif name == 'HeliumNI':
            start_time = time.time()
            for i in range(N_var):
                [x1,x2,accep_ratio[i]] = mp.HeNI(N,alpha[i],h)
                E = E_loc.HeNI(x1,x2,alpha[i])
                E_avg[i] = 1/N*np.sum(E)
                E_var[i] = np.var(E)


            pf.fig_X(alpha, E_avg, E_var, accep_ratio,'HeliumNI')
            print("1--- %s seconds ---" %(time.time() - start_time))
            
        elif name == 'Helium':
            if Nw ==1:
                start_time = time.time()
                for i in range(N_var):
                    [x1,x2,accep_ratio[i]]=mp.He(N,alpha[i],h)
                    E = E_loc.He(x1,x2,alpha[i])
                    E_avg[i] = 1/N*np.sum(E)
                    E_var[i] = np.var(E)


                pf.fig_X(alpha, E_avg, E_var, accep_ratio,'Helium')
                print("1--- %s seconds ---" %(time.time() - start_time))
            elif Nw>1:
                start_time = time.time()
                for i in range(N_var):
                    [x1,x2,accep_ratio[i]] = mp.He2(Nw,N,alpha[i],h)
                    E = E_loc.He2(x1,x2,alpha[i])
                    E_avg[i] = 1/(N*Nw)*np.sum(E)
                    E_var[i] = np.var(E)

    
                pf.fig_X(alpha, E_avg, E_var, accep_ratio,'Helium')
                print("1--- %s seconds ---" %(time.time() - start_time))
            
    elif method == 'min':
        if name == 'HO':               
                start_time = time.time()
                [E_avg,E_var,accep_ratio,alpha] = minimization.HO(N,alpha,h,gamma,e)
                pf.fig_X2(E_avg, E_var, accep_ratio,'HO')
                pr.print_result(E_avg, alpha, accep_ratio, 'HO')
                print("1--- %s seconds ---" %(time.time() - start_time))
        elif name == 'Hydrogen':
                start_time = time.time()
                [E_avg,E_var,accep_ratio,alpha] = minimization.H(N,alpha,h,gamma,e)
                pf.fig_X2(E_avg, E_var, accep_ratio,'Hydrogen')
                pr.print_result(E_avg, alpha, accep_ratio, 'Hydrogen')
                print("1--- %s seconds ---" %(time.time() - start_time))
        elif name == 'Helium': 
                if Nw ==1:
                    start_time = time.time()
                    [E_avg,E_var,accep_ratio,alpha] = minimization.He(N,alpha,h,gamma,e)
                    pf.fig_X2(E_avg, E_var, accep_ratio,'Helium')
                    pr.print_result(E_avg, alpha, accep_ratio, 'Helium')
                    print("1--- %s seconds ---" %(time.time() - start_time))
                elif Nw>1:
                    start_time = time.time()
                    [E_avg,E_var,accep_ratio,alpha] = minimization.He2(Nw,N,alpha,h,gamma,e)
                    pf.fig_X2(E_avg, E_var, accep_ratio,'Helium')
                    pr.print_result(E_avg, alpha, accep_ratio, 'Helium')
                    print("1--- %s seconds ---" %(time.time() - start_time))

        