import numpy as np
from math import exp
import matplotlib.pyplot as plt
import trial_function as tf
import local_energy as E_loc
import metropolis as mp
import minimization
import time

def fig_HO(alpha, E_avg, E_var, accep_ratio):
    """This function generates figures from the computed values for the Harmonic Osillator and plots the exact solution  next to it.
    ------
    Input Parameters:
    alpha: vector with the values of the variational parameter
    E_avg: vector with the average energies corresponding to the values of the variational parameter
    E_var: vector with the variances of the average energies
    accep_ratio:ratio of accepted trials to the number of samples for each value of the variational parameter
    """
    plt.ioff()  
    fig1=plt.figure()
    plt.plot(alpha,E_avg,'bo',markersize=2)
    #The exact relation according to the book of Thijssen
    plt.plot(alpha,(1/2*alpha + 1/(8*alpha)),'--r')
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$E_T$')
    plt.gca().legend(('simulation result','exact result'),loc = 1)
    fig1.savefig('HO_E_avg.pdf', bbox_inches='tight') 
    plt.close(fig1)
    

    fig2=plt.figure()
    plt.plot(alpha,E_var,'bo',markersize=2)
    #The exact relation according to the book of Thijssen
    plt.plot(alpha,(1-4*alpha**2)**2/(32*alpha**2),'--r')
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'variance')
    plt.gca().legend(('simulation result','exact result'),loc = 1)
    fig2.savefig('HO_E_var.pdf', bbox_inches='tight')
    plt.close(fig2)

    fig3=plt.figure()
    plt.plot(alpha,accep_ratio,'bo',markersize=2)
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'acceptance ratio')
    fig3.savefig('HO_accep_ratio.pdf', bbox_inches='tight')
    plt.close(fig3)
    

def fig_H(alpha, E_avg, E_var, accep_ratio):
    """This function generates figures from the computed values for the Hydrogen and plots the exact solution  next to it.
    ------
    Input Parameters:
    alpha: vector with the values of the variational parameter
    E_avg: vector with the average energies corresponding to the values of the variational parameter
    E_var: vector with the variances of the average energies
    accep_ratio:ratio of accepted trials to the number of samples for each value of the variational parameter
    """
    plt.ioff()  
    fig1=plt.figure()
    plt.plot(alpha,E_avg,'bo',markersize=2)
    #The exact relation according to the book of Thijssen
    plt.plot(alpha,(alpha**2/2-alpha),'--r')
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$E_T$')
    plt.gca().legend(('simulation result','exact result'),loc = 1)
    fig1.savefig('Hydrogen_E_avg.pdf', bbox_inches='tight') 
    plt.close(fig1)
    

    fig2=plt.figure()
    plt.plot(alpha,E_var,'bo',markersize=2)
    #The exact relation according to the book of Thijssen
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'variance')
    fig2.savefig('Hydrogen_E_var.pdf', bbox_inches='tight')
    plt.close(fig2)

    fig3=plt.figure()
    plt.plot(alpha,accep_ratio,'bo',markersize=2)
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'acceptance ratio')
    fig3.savefig('Hydrogen_accep_ratio.pdf', bbox_inches='tight')
    plt.close(fig3)
    
def fig_X(alpha, E_avg, E_var, accep_ratio, name):
    """This function generates figures from the computed values for the Helium atoms with predetermined values for alpha.
    ------
    Input Parameters:
    alpha: vector with the values of the variational parameter
    E_avg: vector with the average energies corresponding to the values of the variational parameter
    E_var: vector with the variances of the average energies
    accep_ratio:ratio of accepted trials to the number of samples for each value of the variational parameter
    name: input for the quantum system 
    """
    
    plt.ioff()  
    fig1=plt.figure()
    plt.plot(alpha,E_avg,'bo',markersize=2)
    plt.xlabel(r'$\alpha$')
    plt.ylabel(r'$E_T$')
    fig1.savefig('%s_E_avg.pdf'%name, bbox_inches='tight')
    plt.close(fig1)

    fig2=plt.figure()
    plt.plot(alpha,E_var,'bo',markersize=2)
    plt.xlabel(r'$\alpha$')
    plt.ylabel('variance')
    fig2.savefig('%s_E_var.pdf'%name, bbox_inches='tight')
    plt.close(fig2)

    fig3=plt.figure()
    plt.plot(alpha, accep_ratio,'bo',markersize=2)
    plt.xlabel(r'$\alpha$',)
    plt.ylabel('acceptance ratio')
    fig3.savefig('%s_accep_ratio.pdf'%name, bbox_inches='tight')
    plt.close(fig3)
    
def fig_X2(E_avg, E_var, accep_ratio, name):
    """This function generates figures from the computed values for the Harmonic Oscillator, the Hydrogen and Helium atoms with the use of the minimzation alogrithm.
    ------
    Input Parameters:
    alpha: vector with the values of the variational parameter
    E_avg: vector with the average energies corresponding to the values of the variational parameter
    E_var: vector with the variances of the average energies
    accep_ratio:ratio of accepted trials to the number of samples for each value of the variational parameter
    name: input for the quantum system 
    """
    

    plt.ioff()  
    fig1=plt.figure()
    plt.plot(E_avg,'bo',markersize=2)
    plt.xlabel('minimization iteration')
    plt.ylabel(r'$E_T$')
    fig1.savefig('%s_E_avg_min.pdf'%name, bbox_inches='tight')
    plt.close(fig1)

    fig2=plt.figure()
    plt.plot(E_var,'bo',markersize=2)
    plt.xlabel('minimization iteration')
    plt.ylabel('variance')
    fig2.savefig('%s_E_var_min.pdf'%name, bbox_inches='tight')
    plt.close(fig2)

    fig3=plt.figure()
    plt.plot(accep_ratio,'bo',markersize=2)
    plt.xlabel('minimization iteration')
    plt.ylabel('acceptance ratio')
    fig3.savefig('%s_accep_ratio_min.pdf'%name, bbox_inches='tight')
    plt.close(fig3)    
    
    
    
    


