import numpy as np


def HO(x,alpha):
    """This function gives the local energy of a harmonic oscillator(HO) as function of the position parameter x and the constant alpha. The local     energy is derived using the trial wavefunction exp(-alpha*x**2), which is the standard form of the groundstate of the HO
    
    ---
    Input parameters:
    x = position parameter
    alpha = constant
    
    Output parameters:
    EL: value of the local energy
    
    """
    EL = alpha + np.square(x)*(1/2-2*alpha**2)
    return EL


def H(r,alpha):
    """THis function gives the local energy of a Hydrogen atom as a function of the position parameters r and the constant alpha.
    ---
    Input parameters:
    r = radial position of electron
    alpha = constant

    Output Parameter:
    EL: value of the local energy   
    
    """
    EL = -1/r - 1/2*alpha*(alpha-2/r)
    return EL

def HeNI(x1,x2,alpha):
    """THis function gives the local energy of a Helium atom with non-interacting electronsas a function of the position parameters x1,x2 and the constant alpha.
    ---
    Input parameters:
    x1 = positions of electron 1
    x2 = positions of electron 2
    alpha = constant

    Output Parameter:
    EL: value of the local energy   
    
    """
    
    r1 = np.sqrt(x1[:,0]**2 + x1[:,1]**2 + x1[:,2]**2)
    r2 = np.sqrt(x2[:,0]**2 + x2[:,1]**2 + x2[:,2]**2)
    r12 = np.sqrt((x1[:,0]-x2[:,0])**2 + (x1[:,1]-x2[:,1])**2 + (x1[:,2]-x2[:,2])**2)
    
    EL = -alpha**2 + alpha/r1 + alpha/r2 - 2/r1 - 2/r2 + 1/r12
    return(EL)

def He(X1,X2,alpha):
    """This function gives the local energy of a Helium atom as function of the position parameters X1,X2 and the constant alpha.
        ---
    Input parameters:
    X1 = positions of electron 1
    X2 = positions of electron 2
    alpha = constant
    
        Output Parameter:
    EL: value of the local energy 
    
    """  
    R1 = np.sqrt(X1[:,0]**2+X1[:,1]**2+X1[:,2]**2)
    R2 = np.sqrt(X2[:,0]**2+X2[:,1]**2+X2[:,2]**2)
    R12 =np.sqrt((X1[:,0]-X2[:,0])**2+(X1[:,1]-X2[:,1])**2+(X1[:,2]-X2[:,2])**2) 
    
    EL = -4+np.sum((X1/R1[:,None]-X2/R2[:,None])*(X1-X2),axis = 1)/(R12*(1+alpha*R12)**2)-1/(R12*(1+alpha*R12)**3)-1/(4*(1+alpha*R12)**4)+1/R12   
    return EL

def He2(x1,x2,alpha):
    """This function gives the local energy of a Helium atom as function of the position parameters X1,X2 and the constant alpha. The parameter x is an array with Nw number of walkers, N number of positions for each walker and 3 x,y,z posi21
        ---
    Input parameters:
    x1 = positions of electron 1
    x2 = positions of electron 2
    alpha = constant
    
    Output Parameter:
    EL: value of the local energy 
    
    """  
    r1 = np.sqrt(x1[:,:,0]**2+x1[:,:,1]**2+x1[:,:,2]**2)
    r2 = np.sqrt(x2[:,:,0]**2+x2[:,:,1]**2+x2[:,:,2]**2)
    r12 =np.sqrt((x1[:,:,0]-x2[:,:,0])**2+(x1[:,:,1]-x2[:,:,1])**2+(x1[:,:,2]-x2[:,:,2])**2) 
    
    N = np.shape(x1)
    rr1 = r1.reshape(N[0],N[1],1)
    rr2 = r2.reshape(N[0],N[1],1)
    
    EL = -4+np.sum((x1/rr1-x2/rr2)*(x1-x2),axis = 2)/(r12*(1+alpha*r12)**2)-1/(r12*(1+alpha*r12)**3)-1/(4*(1+alpha*r12)**4)+1/r12   
    return EL