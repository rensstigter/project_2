import numpy as np
from math import exp
import trial_function as tf
import local_energy as E_loc

def metropolis(N,alpha,h):
    """This function generates a sampling for the Harmonic Oscillator according to the metropolis algorithm dependent on the desired distribution by the chosen trial function.
    ----
    Input Parameters:
    N: number of steps the random walker takes.
    alpha: variational parameter
    h: order of stepsize
    
    Output Parameters:
    x: sampling with the desired distribution
    acceptance_ratio: ratio of accepted trials to the number of samples.
    """
    #prescribe position array
    x = np.zeros(N)
    x[0] = (1/(alpha))**(1/2)
    
    N_accepted = 0    
    #generate random walk
    for i in range(N-1):
        #generate new condition in range  x_i-h_i/2< x_trial < x_i+h_i/2 
        x_trial = x[i] + (np.random.rand(1)*h) - 1/2*h
        #print(x_trial) 
        ratio = (tf.HO(x_trial,alpha)/tf.HO(x[i],alpha))**2
        
        if (ratio >= 1):
            x[i+1] = x_trial
            N_accepted = N_accepted+1
        else:
            if (ratio>=np.random.rand(1)):
                x[i+1] = x_trial
                N_accepted = N_accepted+1
            else:
                x[i+1]=x[i]
        
    #Calculate the acceptance ratio: the number of positions accepted divided by the total number of positions. This should be approximately 0.5
    acceptance_ratio = N_accepted/N

    return x, acceptance_ratio


def H(N,alpha,h):
    """This function generates a sampling for the Hydrogen atom according to the metropolis algorithm dependent on the desired distribution by the chosen trial function.
    ----
    Input Parameters:
    N: number of steps the random walker takes.
    alpha: variational parameter
    h: order of stepsize
    
    Output Parameters:
    x: sampling with the desired distribution
    acceptance_ratio: ratio of accepted trials to the number of samples.
    """
    #prescribe position array
    x = np.zeros(N)
    x[0] =(1/alpha)
    
    N_accepted = 0    
    #generate random walk
    for i in range(N-1):
        #generate new condition in range  x_i-h_i/2< x_trial < x_i+h_i/2 
        x_trial = np.abs(x[i] + (np.random.rand(1)*h) - 1/2*h)
        #print(x_trial) 
        ratio = (tf.H(x_trial,alpha)/tf.H(x[i],alpha))**2
        
        if (ratio >= 1):
            x[i+1] = x_trial
            N_accepted = N_accepted+1
        else:
            if (ratio>=np.random.rand(1)):
                x[i+1] = x_trial
                N_accepted = N_accepted+1
            else:
                x[i+1]=x[i]
        
    #Calculate the acceptance ratio: the number of positions accepted divided by the total number of positions. This should be approximately 0.5
    acceptance_ratio = N_accepted/N
    
    return x, acceptance_ratio


def HeNI(N,alpha,h0):
    """This function generates a sampling for the Helium atom with non-interacting electrons according to the metropolis algorithm dependent on the desired distribution by the chosen trial function.
    ----
    Input Parameters:
    N: number of steps the random walker takes.
    alpha: variational parameter
    h0: order of stepsize
    
    Output Parameters:
    x1: sampling with the desired distribution of electron 1
    x2: sampling with the desired distribution of electron 2
    acceptance_ratio: ratio of accepted trials to the number of samples.
    """
    #prescribe position array
    x1 = np.zeros([N,3])
    x2 = np.zeros([N,3])
    x1[0,:] = np.random.rand(3)
    x2[0,:] = np.random.rand(3)
    
    E = np.zeros(N)
    N_accepted = 0 
    
    h = h0/alpha
    
    #generate random walk
    for i in range(N-1):
        #generate new condition in range  x_i-h_i/2< x_trial < x_i+h_i/2 
        x1_trial = x1[i,:] + (np.random.rand(3)*h) - 1/2*h        
        x2_trial = x2[i,:] + (np.random.rand(3)*h) - 1/2*h

        #print(x1_trial) 
        ratio = (tf.HeNI(x1_trial,x2_trial,alpha)/tf.HeNI(x1[i,:],x2[i,:],alpha))**2
        #print(ratio)

        if (ratio >= 1):
            x1[i+1,:] = x1_trial
            x2[i+1,:] = x2_trial
            N_accepted = N_accepted+1
        else:
            if (ratio>=np.random.rand(1)):
                x1[i+1,:] = x1_trial
                x2[i+1,:] = x2_trial
                N_accepted = N_accepted+1
            else:
                x1[i+1] = x1[i]
                x2[i+1] = x2[i]

    acceptance_ratio = N_accepted/N
    return(x1,x2,acceptance_ratio)

def He(N,alpha,h0):
    """This function generates a sampling for the Helium atom  according to the metropolis algorithm dependent on the desired distribution by the chosen trial function.
    ----
    Input Parameters:
    N: number of steps the random walker takes.
    alpha: variational parameter
    h0: order of stepsize
    
    Output Parameters:
    x1: sampling with the desired distribution of electron 1
    x2: sampling with the desired distribution of electron 2
    acceptance_ratio: ratio of accepted trials to the number of samples.
    """
    #prescribe position array
    x1 = np.zeros([N,3])
    x2 = np.zeros([N,3])
    #initial conditions. The length of a bohr-radius is 1, so the initial condition should be around that length.
    x1_0 = np.random.rand(3) 
    x2_0 = np.random.rand(3)
    x1[0,:] = x1_0
    x2[0,:] = x2_0
    E = np.zeros(N)
    N_accepted = 0
      
    h = h0
            
    #generate random walk
    for i in range(N-1):
        #generate new condition in range  x_i-h_i/2< x_trial < x_i+h_i/2 
        x1_trial = x1[i,:] + (np.random.rand(3)*h) - 1/2*h
        x2_trial = x2[i,:] + (np.random.rand(3)*h) - 1/2*h
        
        #print(x_trial) 
        ratio = (tf.trial_He(x1_trial,x2_trial,alpha)/tf.trial_He(x1[i,:],x2[i,:],alpha))**2
        
        if (ratio >= 1):
            x1[i+1,:] = x1_trial
            x2[i+1,:] = x2_trial
            N_accepted = N_accepted+1
        else:
            if (ratio>=np.random.rand(1)):
                x1[i+1,:] = x1_trial
                x2[i+1,:] = x2_trial
                N_accepted = N_accepted+1
            else:
                x1[i+1,:] = x1[i,:]
                x2[i+1,:] = x2[i,:]
        
    #Calculate the acceptance ratio: the number of positions accepted divided by the total number of positions. This should be approximately 0.5
    acceptance_ratio = N_accepted/N

    return x1,x2, acceptance_ratio


#Metropolis algorithm for the helium atom, but now with the possibility to add some 'walkers'. 
def He2(Nw,N,alpha,h0):
    """This function generates a sampling for the Helium atom with non-interacting electrons according to the metropolis algorithm dependent on the desired distribution by the chosen trial function using multiple walkers.
    ----
    Input Parameters:
    Nw: number of random walkers
    N: number of steps the random walker takes.
    alpha: variational parameter
    h0: order of stepsize
    
    Output Parameters:
    x1: sampling with the desired distribution of electron 1
    x2: sampling with the desired distribution of electron 2
    acceptance_ratio: ratio of accepted trials to the number of samples.
    """
    #prescribe position array
    x1 = np.zeros([Nw,N,3])
    x2 = np.zeros([Nw,N,3])
    h = h0
    
    #intial conditions. x_0 between -1/2 and 1/2 because the function goes with exp(2r). So 2r = 1, r = 1/2. 
    x1[:,0,:] = (np.random.rand(Nw,3)-1/2)*h
    x2[:,0,:] = (np.random.rand(Nw,3)-1/2)*h
    
    E = np.zeros(N)
    N_accepted = 0
    
    #generate random walk
    for i in range(N-1):
        #generate new condition in range  x_i-h_i/2< x_trial < x_i+h_i/2 
        x1_trial = x1[:,i,:] + (np.random.rand(Nw,3)-1/2)*h
        x2_trial = x2[:,i,:] + (np.random.rand(Nw,3)-1/2)*h
        
        #print(x_trial) 
        ratio = (tf.trial_He2(x1_trial,x2_trial,alpha)/tf.trial_He2(x1[:,i,:],x2[:,i,:],alpha))**2
        accepted = np.any(ratio.reshape(Nw,1)>np.random.rand(Nw,1), axis = 1).reshape(Nw,1)
        N_accepted = N_accepted + np.sum(accepted)
        x1[:,i+1,:] = accepted*x1_trial
        x2[:,i+1,:] = accepted*x2_trial
        
        not_accepted = np.abs(accepted-1)
        x1[:,i+1,:] = x1[:,i+1,:] + not_accepted*x1[:,i,:]
        x2[:,i+1,:] = x2[:,i+1,:] + not_accepted*x2[:,i,:]
        
        h = (np.sum(accepted)/Nw - 1/2)  + h

    #Calculate the acceptance ratio: the number of positions accepted divided by the total number of positions. This should be approximately 0.5
    acceptance_ratio = N_accepted/(N*Nw)

    return x1,x2, acceptance_ratio



