import numpy as np
from math import exp,sqrt
import trial_function as tf
import local_energy as E_loc
import metropolis as mp

def HO(N,alpha0,h0,gamma,e):
    """This function calculates the minimum value for the energy, energy variance and alpha of the Harmonic Ocillator with the use of the    minimaztion algorithm and with 1 random walker.
    -----
    Input Parameters:
    N: number of steps taken by the random walker     
    alpha0: starting point of the variational parameter (scalar)  
    h0: order of stepsize (scalar) 
    gamma: minimization step size multiplier  
    e: desired precision of the result
    
    Output Parameters:
    E_avg: the average energy obtained for each iteration of the minimization procedure
    E_var: value of the variance of the average energy obtained for each iteration of the minimization procedure
    accep_ratio: acceptance ratio of the samples by the metropolis algorithm for the sampling used in eacht iteration of the minization procedure
    alpha: all values of alpha used in the minimzation procedure 
    
    """
    E_avg = np.zeros(1)
    E_var = np.zeros(1)
    accep_ratio = np.zeros(1)
    alpha = np.zeros(1)
    alpha[0] = alpha0
    i = 0
    deriv_E_alpha = 1
    
    while np.abs(deriv_E_alpha) > e:
        h = (h0/alpha[i])**(1/2)
        [x,accep] = mp.metropolis(N,alpha[i],h)
        E = E_loc.HO(x,alpha[i])
        avg = 1/N*np.sum(E)
        var = np.var(E)
        
        A = np.sum(E_loc.HO(x,alpha[i])*(-x**2))
        B = np.sum(-x**2)
        
        deriv_E_alpha = 2/N*(A-avg*B)
        alpha_new = alpha[i] - gamma*deriv_E_alpha
        alpha = np.append(alpha,alpha_new)
        
        E_avg = np.append(E_avg,avg)
        E_var = np.append(E_var,var)
        accep_ratio = np.append(accep_ratio,accep)
        
        i = i+1
        
    return E_avg, E_var, accep_ratio, alpha

def H(N,alpha0,h0,gamma,e):
    """This function calculates the minimum value for the energy, energy variance and alpha of the Hydrogen atom with the use of the    minimaztion algorithm and with 1 random walker.
    -----
    Input Parameters:
    N: number of steps taken by the random walker     
    alpha0: starting point of the variational parameter (scalar)  
    h0: order of stepsize (scalar) 
    gamma: minimization step size multiplier  
    e: desired precision of the result
    
    Output Parameters:
    E_avg: the average energy obtained for each iteration of the minimization procedure
    E_var: value of the variance of the average energy obtained for each iteration of the minimization procedure
    accep_ratio: acceptance ratio of the samples by the metropolis algorithm for the sampling used in eacht iteration of the minization procedure
    alpha: all values of alpha used in the minimzation procedure 
    
    """
    E_avg = np.zeros(1)
    E_var = np.zeros(1)
    accep_ratio = np.zeros(1)
    alpha = np.zeros(1)
    alpha[0] = alpha0
    i = 0
    deriv_E_alpha = 1
    
    while np.abs(deriv_E_alpha) > e:
        h = (h0/alpha[i])
        [r,accep] = mp.H(N,alpha[i],h)
        E = E_loc.H(r,alpha[i])
        avg = 1/N*np.sum(E)
        var = np.var(E)
        
        A = np.sum(E_loc.H(r,alpha[i])*(-r))
        B = np.sum(-r)
        
        deriv_E_alpha = 2/N*(A-avg*B)
        alpha_new = alpha[i] - gamma*deriv_E_alpha
        alpha = np.append(alpha,alpha_new)
        
        E_avg = np.append(E_avg,avg)
        E_var = np.append(E_var,var)
        accep_ratio = np.append(accep_ratio,accep)
        
        i = i+1
        #print(deriv_E_alpha)
    return E_avg, E_var, accep_ratio, alpha

def He(N,alpha0,h0,gamma,e): 
    """This function calculates the minimum value for the energy, energy variance and alpha of the Helium atom with the use of the    minimaztion algorithm and with 1 random walker.
    -----
    Input Parameters:
    N: number of steps taken by the random walker     
    alpha0: starting point of the variational parameter (scalar)  
    h0: order of stepsize (scalar) 
    gamma: minimization step size multiplier  
    e: desired precision of the result
    
    Output Parameters:
    E_avg: the average energy obtained for each iteration of the minimization procedure
    E_var: value of the variance of the average energy obtained for each iteration of the minimization procedure
    accep_ratio: acceptance ratio of the samples by the metropolis algorithm for the sampling used in eacht iteration of the minization procedure
    alpha: all values of alpha used in the minimzation procedure 
    
    """
    E_avg = np.zeros(1)
    E_var = np.zeros(1)
    accep_ratio = np.zeros(1)
    alpha = np.zeros(1)
    alpha[0] = alpha0
    i = 0
    deriv_E_alpha = 1
    
    while np.abs(deriv_E_alpha) > e:
        [x1,x2,accep] = mp.He(N,alpha[i],h0)
        E = E_loc.He(x1,x2,alpha[i])
        avg = 1/N*np.sum(E)
        var = np.var(E)
        
        R12 = np.sqrt(np.sum((x1-x2)**2,axis=1))
        A = np.sum(E_loc.He(x1,x2,alpha[i])*(-1/2)*(R12/(1+alpha[i]*R12))**2)
        B = np.sum(-1/2*(R12/(1+alpha[i]*R12))**2)
        
        deriv_E_alpha = 2/N*(A-avg*B)
        alpha_new = alpha[i] - gamma*deriv_E_alpha
        alpha = np.append(alpha,alpha_new)
        
        E_avg = np.append(E_avg,avg)
        E_var = np.append(E_var,var)
        accep_ratio = np.append(accep_ratio,accep)
        
        i = i+1
        
    return E_avg, E_var, accep_ratio, alpha

def He2(Nw,N,alpha0,h0,gamma,e):  
    """This function calculates the minimum value for the energy, energy variance and alpha of the Helium at with the use of the minimaztion algorithm and with multiple random walkers.
    -----
    Input Parameters:
    Nw: number of random walkers
    N: number of steps taken by the random walker  
    alpha0: starting point of the variational parameter (scalar)  
    h0: order of stepsize (scalar) 
    gamma: minimization step size multiplier  
    e: desired precision of the result
    
    Output Parameters:
    E_avg: the average energy obtained for each iteration of the minimization procedure
    E_var: value of the variance of the average energy obtained for each iteration of the minimization procedure
    accep_ratio: acceptance ratio of the samples by the metropolis algorithm for the sampling used in eacht iteration of the minization procedure
    alpha: all values of alpha used in the minimzation procedure 
    
    """
    E_avg = np.zeros(1)
    E_var = np.zeros(1)
    accep_ratio = np.zeros(1)
    alpha = np.zeros(1)
    alpha[0] = alpha0
    i = 0
    deriv_E_alpha = 1
        
    while np.abs(deriv_E_alpha) > e:
        [x1,x2,accep] = mp.He2(Nw,N,alpha[i],h0)
        E = E_loc.He2(x1,x2,alpha[i])
        avg = 1/(N*Nw)*np.sum(E)
        var = np.var(E)
        
        r12 =np.sqrt((x1[:,:,0]-x2[:,:,0])**2+(x1[:,:,1]-x2[:,:,1])**2+(x1[:,:,2]-x2[:,:,2])**2) 
        A = np.sum(E_loc.He2(x1,x2,alpha[i])*(-1/2)*(r12/(1+alpha[i]*r12))**2)
        B = np.sum(-1/2*(r12/(1+alpha[i]*r12))**2)
        
        deriv_E_alpha = 2/(N*Nw)*(A-avg*B)
        alpha_new = alpha[i] - gamma*deriv_E_alpha
        alpha = np.append(alpha,alpha_new)
        
        E_avg = np.append(E_avg,avg)
        E_var = np.append(E_var,var)
        accep_ratio = np.append(accep_ratio,accep)
        
        i = i+1
        
    return E_avg, E_var, accep_ratio, alpha