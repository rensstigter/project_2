import numpy as np
from math import sqrt

def HO(x,alpha):
    """This function gives a trial wavefunction, as a function of the position parameter x and the constant alpha.
    
    ---
    Input parameters:
    x = position parameter
    alpha = constant
    
    Output parameter:
    gamma: values of the trial wave function for the input parameters.
    """
    gamma = np.exp(-alpha*x**2)
    return gamma


def H(r,alpha):
    """This function gives a trial wavefunction for the Hydrogen atom, as a function of the position parameter r and the constant alpha.
    
    ---
    Input parameters:
    r = position parameter
    alpha = constant
    
    Output parameter:
    gamma: values of the trial wave function for the input parameters.
    """
    gamma = r*np.exp(-alpha*r)
    return gamma

def HeNI(x1,x2,alpha):
    """This function gives a trial wavefunction for Helium with non-interacting electrons
    ---
    Input parameters:
    x1 = position parameter of electron 1
    x2 = position parameter of electron 2
    alpha = constant
    
    Output parameter:
    gamma: values of the trial wave function for the input parameters.
    """
    
    r1 = np.sqrt(x1[0]**2 + x1[1]**2 + x1[2]**2)
    r2 = np.sqrt(x2[0]**2 + x2[1]**2 + x2[2]**2)
    
    gamma = np.exp(-alpha*(r1+r2))
    return gamma

def trial_He(X1,X2,alpha):
    """This function gives a trial wavefunction, as a function of the radii R1,R2 and the difference between those and the constant alpha.
    
    ---
    Input parameters:
    R1 = radius from electron 1 to origin
    R2 = radius from electron 2 to origin
    R12 = length of difference vector between electron 1 and electron 2
    alpha = constant
    
    Output parameter:
    gamma: values of the trial wave function for the input parameters.
    """
    
    R1 = np.sqrt(X1[0]**2+X1[1]**2+X1[2]**2)
    R2 = np.sqrt(X2[0]**2+X2[1]**2+X2[2]**2)
    R12 =np.sqrt((X1[0]-X2[0])**2+(X1[1]-X2[1])**2+(X1[2]-X2[2])**2) 
    
    trial_He = np.exp(-2*(R1+R2))*np.exp(R12/(2*(1+alpha*R12)))
    
    return trial_He

def trial_He2(X1,X2,alpha):
    """This function gives a trial wavefunction, as a function of the radii R1,R2 and the difference between those and the constant alpha.
    
    ---
    Input parameters:
    R1 = radius from electron 1 to origin
    R2 = radius from electron 2 to origin
    R12 = length of difference vector between electron 1 and electron 2
    alpha = constant
    
    Output parameter:
    gamma: values of the trial wave function for the input parameters.
    """
    
    R1 = np.sqrt(X1[:,0]**2+X1[:,1]**2+X1[:,2]**2)
    R2 = np.sqrt(X2[:,0]**2+X2[:,1]**2+X2[:,2]**2)
    R12 =np.sqrt((X1[:,0]-X2[:,0])**2+(X1[:,1]-X2[:,1])**2+(X1[:,2]-X2[:,2])**2) 
    
    trial_He = np.exp(-2*(R1+R2))*np.exp(R12/(2*(1+alpha*R12)))
    return trial_He

