import numpy as np

def print_result(E_avg,alpha,accep_ratio,name):
    """ Find the end values for the energy and for alpha. Also check if the acceptance ratio is indeed approximately 0.5. Prints these results to a file.
    ----
    Input Parameters:
    alpha: vector with the values of the variational parameter
    E_avg: vector with the average energies corresponding to the values of the variational parameter
    accep_ratio:ratio of accepted trials to the number of samples for each value of the variational parameter
    name: input for the quantum system 
    
    """ 
    file = open('%s.txt'%(name),'w+')
    file.write('Ground state energy:%f \n'%(E_avg[-1]))
    file.write('Variational parameter:%f \n'%(alpha[-1]))
    file.write('Mean acceptance ratio:%f \n'%(np.mean(accep_ratio[1:])))
    file.flush()
    file.close